# README

* Ruby version - **2.5.5p157**

* Rails version - **5.2.4.1**

* Bundler version - **1.17.3**

***
* bundle install

* rails db:drop db:create db:migrate db:seed
***

-[x] rubocop

-[x] rspec

-[x] brakeman

-[x] bundle-audit
