class ApplicationController < ActionController::API
  include Knock::Authenticable
  include ErrorHandleable

  after_action :refresh_bearer_auth_header, if: :bearer_auth_header_present

  protected

  def bearer_auth_header_present
    request.env['HTTP_AUTHORIZATION'] =~ /Bearer/
  end

  def auth_token
    # Remove 'Bearer ' from the Authorization of header
    token = request.headers['Authorization'][7..-1]
    Knock::AuthToken.new(token: token)
  end

  private

  def refresh_bearer_auth_header
    return unless current_user && current_user&.need_refresh_token?(auth_token.payload)

    headers['Access-Control-Expose-Headers'] = 'Authorization'
    headers['Authorization'] = current_user.set_new_token
  end
end
