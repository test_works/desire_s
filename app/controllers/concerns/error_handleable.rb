module ErrorHandleable
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

    protected

    def render_unprocessable_entity_response(exception)
      render json: {
        message: 'Validation Failed',
        errors: ErrorsSerializer.new(exception.record).serialize
      }, status: :unprocessable_entity
    end

    def render_not_found_response
      render json: { field: 'resource', code: 'not_found' }, status: :not_found
    end

    def render_forbidden_response
      render json: { field: 'code', code: 'forbidden' }, status: :forbidden
    end

    def render_expired_response
      render json: { errors: { field: 'code', code: 'expired' }, message: 'Validation Failed' }, status: :unprocessable_entity
    end

    def render_invalid_params_response
      render json: { errors: { field: 'code', code: 'expired' }, message: 'Invalid Params' }, status: :unprocessable_entity
    end

    def respond_with_errors(obj)
      render json: {
        message: 'Validation Failed',
        errors: ErrorsSerializer.new(obj).serialize
      }, status: :unprocessable_entity
    end
  end
end
