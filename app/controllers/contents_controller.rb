class ContentsController < ApplicationController
  before_action :authenticate_user, only: [:create, :update, :destroy, :favorite]
  before_action :set_content, only: [:show, :update, :destroy, :favorite]

  def index
    @contents = Content.published
    render json: ContentSerializer.new(@contents).serialized_json
  end

  def show
    if @content.published? || current_user&.contents&.include?(@content)
      @contents_users_info = ContentsUsersInfo.find_or_initialize_by(user: current_user, content: @content)
      @contents_users_info.update(read: true) if current_user && !current_user.read_contents.include?(@content)

      render json: ContentSerializer.new(@content).serialized_json
    else
      render_not_found_response
    end
  end

  def create
    @content = Content.new(content_params
                             .merge(user: current_user))
    if @content.save
      render json: ContentSerializer.new(@content).serialized_json
    else
      respond_with_errors(@content)
    end
  end

  def update
    if current_user.contents.include?(@content)
      if @content.update(content_params)
        render json: ContentSerializer.new(@content).serialized_json
      else
        respond_with_errors(@content)
      end
    else
      render_not_found_response
    end
  end

  def destroy
    if current_user.contents.include?(@content)
      if @content.destroy
        render json: { message: :ok }, status: :ok
      else
        respond_with_errors(@content)
      end
    else
      render_not_found_response
    end
  end

  def unread
    @contents = current_user ? current_user.unread_contents : Content.published
    render json: ContentSerializer.new(@contents).serialized_json
  end

  def favorite
    @contents_users_info = ContentsUsersInfo.find_or_initialize_by(user: current_user, content: @content)
    if @contents_users_info.update(favorite: true)
      render json: { message: :ok }, status: :ok
    else
      respond_with_errors(@contents_users_info)
    end
  end

  private

  def set_content
    @content = Content.find(params[:id])
  end

  def content_params
    params.permit(*whitelisted)
  end

  def whitelisted
    [
      :title,
      :announcement,
      :body,
      :status,
    ]
  end
end
