class UserTokenController < Knock::AuthTokenController
  skip_forgery_protection

  def create
    @token = auth_token
    @user = entity
    if @user.present? && @user.errors.empty?
      render json: UserTokenSerializer.new(@user, params: { token: @token.token }).serialized_json
    else
      render json: {
        message: 'User Authorization Failed',
        errors: ErrorsSerializer.new(@user).serialize
      }, status: :unauthorized
    end
    render json: { message: 'User Not Found' }, status: :not_found if @user.blank?
  end

  private

  def entity
    @entity ||=
      if entity_class.respond_to?(:from_token_request)
        entity_class.from_token_request(request)
      else
        entity_class.find_by(login: auth_params[:login])
      end
  end

  def auth_params
    params.permit([:login, :password])
  end
end
