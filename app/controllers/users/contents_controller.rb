module Users
  class ContentsController < ApplicationController
    before_action :set_user, only: [:index]

    def index
      @contents = current_user == @user ? @user.contents : @user.contents.published
      render json: ContentSerializer.new(@contents).serialized_json
    end

    private

    def set_user
      @user = User.find(params[:user_id])
    end
  end
end
