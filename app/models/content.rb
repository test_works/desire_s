class Content < ApplicationRecord
  # Associations
  belongs_to :user
  has_many :contents_users_infos, dependent: :destroy
  has_many :users, through: :contents_users_infos

  # Callbacks
  before_validation :normalize_title
  before_validation :normalize_announcement

  # Enums
  enum status: {
    draft: 0,
    published: 1
  }

  # Validations
  validates :title, presence: true, length: { minimum: 1, maximum: 30 }
  validates :announcement, presence: true, length: { minimum: 1, maximum: 30 }
  validates :body, presence: true, length: { minimum: 3, maximum: 65_536 }

  private

  def normalize_title
    self.title = title&.capitalize
  end

  def normalize_announcement
    self.announcement = announcement&.capitalize
  end
end
