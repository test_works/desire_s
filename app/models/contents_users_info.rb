class ContentsUsersInfo < ApplicationRecord
  # Associations
  belongs_to :user
  belongs_to :content
end
