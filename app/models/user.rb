class User < ApplicationRecord
  include Authenticatable

  # Attributes
  has_secure_password
  attr_accessor :token

  # Associations
  has_many :contents, dependent: :destroy
  has_many :contents_users_infos, dependent: :destroy
  has_many :read_contents, ->{ where('contents_users_infos.read = ?', true) }, through: :contents_users_infos, source: :content
  has_many :favorite_contents, ->{ where('contents_users_infos.favorite = ?', true) }, through: :contents_users_infos, source: :content

  # Callbacks
  before_validation :normalize_full_name
  before_create :set_password

  # Validations
  validates :login, presence: true, uniqueness: true, length: { minimum: 3, maximum: 20 }
  validates :full_name, presence: true, length: { minimum: 11, maximum: 70 }
  validates :signature, allow_blank: true, length: { maximum: 255 }

  # Instance methods
  def unread_contents
    Content.where.not(id: read_content_ids).published
  end

  private

  def normalize_full_name
    self.full_name = full_name&.split&.map(&:capitalize)&.join(' ')
  end

  def set_password
    return if Rails.env.test?

    self.password = generate_password
  end

  def generate_password
    SecureRandom.alphanumeric(8)
  end
end
