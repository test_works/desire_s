class ContentSerializer
  include FastJsonapi::ObjectSerializer
  set_key_transform :camel_lower

  attributes :title,
             :announcement,
             :body,
             :status,
             :user_id
end
