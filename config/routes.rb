Rails.application.routes.draw do
  post 'sessions' => 'user_token#create', defaults: { format: :json }

  resources :users, only: [:index], defaults: { format: :json } do
    scope module: :users do
      resources :contents, only: [:index]
    end
  end

  namespace :contents, defaults: { format: :json } do
    get :unread
  end

  resources :contents, defaults: { format: :json } do
    member do
      post :favorite
    end
  end
end
