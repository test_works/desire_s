class CreateContents < ActiveRecord::Migration[5.2]
  def change
    create_table :contents do |t|
      t.string :title
      t.string :announcement
      t.text :body
      t.integer :status, default: 0
      t.references :user, index: true

      t.timestamps
    end
  end
end
