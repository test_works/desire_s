class CreateContentsUsersInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :contents_users_infos do |t|
      t.boolean :favorite
      t.boolean :read
      t.belongs_to :user
      t.belongs_to :content

      t.timestamps
    end
  end
end
