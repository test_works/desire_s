FactoryBot.define do
  factory :content do
    title { Faker::Lorem.word }
    announcement { Faker::Lorem.word }
    body { Faker::Lorem.sentence }
    status { :published }

    user
  end

  trait :as_draft do
    status { :draft }
  end
end
