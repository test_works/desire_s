FactoryBot.define do
  factory :contents_users_info do
    read { false }
    favorite { false }

    user
    content
  end

  trait :as_read do
    read { true }
  end

  trait :as_favorite do
    favorite { true }
  end

  trait :as_read_and_favorite do
    read { true }
    favorite { true }
  end
end
