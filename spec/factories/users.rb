FactoryBot.define do
  factory :user do
    login { Faker::Lorem.characters(number: 10) }
    password { 'develop' }
    full_name { Faker::Name.last_name + ' ' + Faker::Name.first_name + ' ' + Faker::Name.middle_name }
    signature { Faker::Lorem.characters(number: 10) }
  end
end
