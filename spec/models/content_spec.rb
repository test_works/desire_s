RSpec.describe Content, type: :model do
  it { expect(build(:content)).to be_valid }
  it { expect(build(:content, :as_draft)).to be_valid }

  let!(:described_class_object) { create(:content) }

  describe 'ActiveModel' do
    context 'associations' do
      it { expect(described_class_object).to belong_to(:user) }
      it { expect(described_class_object).to have_many(:contents_users_infos).dependent(:destroy) }
      it { expect(described_class_object).to have_many(:users).through(:contents_users_infos) }
    end

    context 'callbacks' do
      it { expect(described_class_object).to callback(:normalize_title).before(:validation) }
      it { expect(described_class_object).to callback(:normalize_announcement).before(:validation) }
    end

    context 'enums' do
      it { expect(described_class_object).to define_enum_for(:status) }
    end

    context 'validations' do
      it { expect(described_class_object).to validate_presence_of(:title) }
      it { expect(described_class_object).to validate_length_of(:title).is_at_least(1).is_at_most(30) }
      it { expect(described_class_object).to validate_presence_of(:announcement) }
      it { expect(described_class_object).to validate_length_of(:announcement).is_at_least(1).is_at_most(30) }
      it { expect(described_class_object).to validate_presence_of(:body) }
      it { expect(described_class_object).to validate_length_of(:body).is_at_least(3).is_at_most(65_536) }
    end
  end
end
