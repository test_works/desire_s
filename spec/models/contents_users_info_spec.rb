RSpec.describe ContentsUsersInfo, type: :model do
  it { expect(build(:contents_users_info)).to be_valid }
  it { expect(build(:contents_users_info, :as_read)).to be_valid }
  it { expect(build(:contents_users_info, :as_favorite)).to be_valid }
  it { expect(build(:contents_users_info, :as_read_and_favorite)).to be_valid }

  let!(:described_class_object) { create(:contents_users_info) }

  describe 'ActiveModel' do
    context 'associations' do
      it { expect(described_class_object).to belong_to(:user) }
      it { expect(described_class_object).to belong_to(:content) }
    end
  end
end
