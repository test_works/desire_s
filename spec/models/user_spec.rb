RSpec.describe User, type: :model do
  it { expect(build(:user)).to be_valid }

  let!(:described_class_object) { create(:user) }

  describe 'ActiveModel' do
    context 'associations' do
      it { expect(described_class_object).to have_many(:contents).dependent(:destroy) }
      it { expect(described_class_object).to have_many(:contents_users_infos).dependent(:destroy) }
      it { expect(described_class_object).to have_many(:read_contents).through(:contents_users_infos).source(:content) }
      it { expect(described_class_object).to have_many(:favorite_contents).through(:contents_users_infos).source(:content) }
    end

    context 'callbacks' do
      it { expect(described_class_object).to callback(:normalize_full_name).before(:validation) }
      it { expect(described_class_object).to callback(:set_password).before(:create) }
    end

    context 'validations' do
      it { expect(described_class_object).to validate_presence_of(:login) }
      it { expect(described_class_object).to validate_uniqueness_of(:login) }
      it { expect(described_class_object).to validate_length_of(:login).is_at_least(3).is_at_most(20) }
      it { expect(described_class_object).to validate_presence_of(:full_name) }
      it { expect(described_class_object).to validate_length_of(:full_name).is_at_least(11).is_at_most(70) }
      it { expect(described_class_object).to allow_value('', nil).for(:signature) }
      it { expect(described_class_object).to validate_length_of(:signature).is_at_most(255) }
    end

    context 'public instance methods' do
      it { expect(described_class_object).to respond_to(:unread_contents) }
    end
  end
end
