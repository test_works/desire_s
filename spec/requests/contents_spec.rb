RSpec.describe Content, type: :request do
  let!(:user) { create(:user) }
  let!(:content) { create(:content) }
  let!(:content_as_draft) { create(:content, :as_draft) }

  describe 'GET #index' do
    context 'with valid attributes' do
      before(:each) { get contents_path }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('contents') }
    end
  end

  describe 'GET #show' do
    context 'with valid attributes' do
      before(:each) { get content_path(content) }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('content') }
    end

    context 'draft content with auth owner user' do
      before(:each) { get content_path(content_as_draft), headers: auth_headers(content_as_draft.user) }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('content') }
    end

    context 'draft content with auth another user' do
      before(:each) { get content_path(content_as_draft), headers: auth_headers(user) }
      it { expect(response).to have_http_status(:not_found) }
    end

    context 'draft content without auth user' do
      before(:each) { get content_path(content_as_draft) }
      it { expect(response).to have_http_status(:not_found) }
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      before(:each) { post contents_path(attributes_for(:content)), headers: auth_headers(user) }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('content') }
    end

    context 'with invalid attributes' do
      before(:each) { post contents_path(title: ''), headers: auth_headers(user) }
      it { expect(response).to have_http_status(:unprocessable_entity) }
    end

    context 'without auth user' do
      before(:each) { post contents_path(attributes_for(:content)) }
      it { expect(response).to have_http_status(:unauthorized) }
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before(:each) { put content_path(content, attributes_for(:content)), headers: auth_headers(content.user) }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('content') }
    end

    context 'with invalid attributes' do
      before(:each) { put content_path(content, title: ''), headers: auth_headers(content.user) }
      it { expect(response).to have_http_status(:unprocessable_entity) }
    end

    context 'with invalid auth user' do
      before(:each) { put content_path(content, attributes_for(:content)), headers: auth_headers(user) }
      it { expect(response).to have_http_status(:not_found) }
    end

    context 'without auth user' do
      before(:each) { put content_path(content, attributes_for(:content)) }
      it { expect(response).to have_http_status(:unauthorized) }
    end
  end

  describe 'DELETE #destroy' do
    context 'with auth user' do
      before(:each) { delete content_path(content), headers: auth_headers(content.user) }
      it { expect(response).to have_http_status(:ok) }
    end

    context 'with invalid auth user' do
      before(:each) { delete content_path(content), headers: auth_headers(user) }
      it { expect(response).to have_http_status(:not_found) }
    end

    context 'without auth user' do
      before(:each) { delete content_path(content) }
      it { expect(response).to have_http_status(:unauthorized) }
    end
  end

  describe 'GET #unread' do
    context 'with auth user' do
      before(:each) { get contents_unread_path, headers: auth_headers(content.user) }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('contents') }
    end

    context 'without auth user' do
      before(:each) { get contents_unread_path }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('contents') }
    end
  end

  describe 'POST #favorite' do
    context 'with owner auth user' do
      before(:each) { post favorite_content_path(content), headers: auth_headers(content.user) }
      it { expect(response).to have_http_status(:ok) }
    end

    context 'with another auth user' do
      before(:each) { post favorite_content_path(content), headers: auth_headers(user) }
      it { expect(response).to have_http_status(:ok) }
    end

    context 'without auth user' do
      before(:each) { post favorite_content_path(content) }
      it { expect(response).to have_http_status(:unauthorized) }
    end
  end
end
