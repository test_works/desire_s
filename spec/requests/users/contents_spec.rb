RSpec.describe Content, type: :request do
  let!(:user) { create(:user) }
  let!(:content) { create(:content) }
  let!(:content_as_draft) { create(:content, :as_draft) }

  describe 'GET #index' do
    context 'with valid attributes' do
      before(:each) { get user_contents_path(user) }
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to match_response_schema('contents') }
    end

    context 'with invalid attributes' do
      before(:each) { get user_contents_path(0) }
      it { expect(response).to have_http_status(:not_found) }
    end
  end
end
